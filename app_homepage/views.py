# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Sum, Count, F
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from app_homepage.models import *
from .forms import *

AUDIO_FILE_TYPES = ['wav', 'mp3', 'ogg']

def homepage_parameters(request):
    user_likes = []
    comments = []
    flag = False
    for song in Song.objects.all():
        comments.append(Comment.objects.filter(song=song).count())
        for user in song.likes.all():
            if user == request.user:
                flag = True
                break
        if flag:
            user_likes += ['True']
        else:
            user_likes += ['False']
        flag = False

    return zip(Song.objects.all(), user_likes, comments)

def profile_parameters(request, song_id):
    user_likes = []
    flag = False
    for user in Song.objects.get(song_id).likes.all():
        if user == request.user:
            flag = True
            break
        if flag:
            user_likes += ['True']
        else:
            user_likes += ['False']
        flag = False

    return zip(Song.objects.all(), user_likes)

def homepage(request):
    context = {}
    context['songs'] = homepage_parameters(request)
    context['comments'] = Comment.objects.all()
    return render(request, 'index.html', context)

def profile(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    context = {}
    context['user_info'] = User.objects.get(username=request.user)
    context['songs'] = Song.objects.filter(user__username=request.user)
    context['uploaded_songs'] = Song.objects.filter(user__username=request.user).count()
    # context['user_info'] = User.objects.get(username=request.user)

    try:
        context['playlist'] = Playlist.objects.get(playlist_user=request.user).playlist_songs.all()
    except:
        pass
    return render(request, 'Profile.html', context)

def otherProfile(request, profile_name):
    context = {}
    user = User.objects.get(username__exact=profile_name)
    context['user_info'] = user
    context['songs'] = Song.objects.filter(user__username=user)
    context['uploaded_songs'] = Song.objects.filter(user__username=user).count()
    context['playlist'] = -1
    return render(request, 'Profile.html', context)

def register(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/')

    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                context = {}
                context['songs'] = homepage_parameters(request)
                return render(request, 'index.html', context)
    context = {
        "form": form,
    }
    return render(request, 'registration.html', context)

def login_user(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                context = {}
                context['songs'] = homepage_parameters(request)
                return HttpResponseRedirect('/', context)
            else:
                return render(request, 'login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'login.html', {'error_message': 'Invalid login'})
    return render(request, 'login.html')

def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'login.html', context)

def upload_song(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    form = SongForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        if len(str(form.cleaned_data['song_author'])) > 60:
            context = {
                'form' : form,
                'error_message': 'Song author field text size cannot be greater than 60 symbols'
            }
            return render(request, 'upload_song.html', context)

        if len(str(form.cleaned_data['song_author'])) > 60:
            context = {
                'form' : form,
                'error_message': 'Song title field text size cannot be greater than 60 symbols'
            }
            return render(request, 'upload_song.html', context)

        song = form.save(commit=False)
        song.song_file = request.FILES['song_file']
        song.song_author = form.cleaned_data['song_author']
        song.song_genre = form.cleaned_data['song_genre']
        song.user = request.user
        file_type = song.song_file.url.split('.')[-1]
        file_type = file_type.lower()

        if file_type not in AUDIO_FILE_TYPES:
            context = {
                'form': form,
                'error_message': 'Audio file must be WAV, MP3, or OGG',
            }
            return render(request, 'upload_song.html', context)

        song.save()
        return redirect('/', {'songs': Song.objects.all()})
    context = {
        'form': form,
    }
    return render(request, 'upload_song.html', context)

def delete_song(request, song_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    context = {}
    song = Song.objects.get(pk=song_id)
    if str(song.user) == str(request.user) or str(request.user) == 'admin':
        song.delete()
    else:
        return HttpResponseRedirect('/')

    context['user_info'] = User.objects.get(username__exact=request.user)
    context['songs'] = Song.objects.filter(user__username=request.user)
    context['uploaded_songs'] = Song.objects.filter(user__username=request.user).count()
    return HttpResponseRedirect('/profile/', context)

def edit_song(request, song_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    song = Song.objects.get(pk=song_id)
    form = SongEditForm(request.POST or None, initial={'song_author': song.song_author, 'song_title': song.song_title, 'song_genre': song.song_genre})
    if form.is_valid():
        song.song_author = form.cleaned_data['song_author']
        song.song_title = form.cleaned_data['song_title']
        song.song_genre = form.cleaned_data['song_genre']
        song.save()

        context = {}
        context['user_info'] = User.objects.get(username__exact=request.user)
        context['songs'] = Song.objects.filter(user__username=request.user)
        context['uploaded_songs'] = Song.objects.filter(user__username=request.user).count()
        try:
            context['playlist'] = Playlist.objects.get(playlist_user=request.user).playlist_songs.all()
        except:
            pass
        return HttpResponseRedirect('/profile/', context)

    context = {
        'form': form,
        'song': song
    }
    return render(request, 'editSong.html', context)

def playlist(request):
    try:
         return render(request, 'Playlist.html', {'playlist': Playlist.objects.get(playlist_user=request.user).playlist_songs.all()})
    except:
        if not request.user.is_authenticated:
            return HttpResponseRedirect('/')
        context = {}
        context['songs'] = Song.objects.filter(user__username=request.user)
        context['uploaded_songs'] = Song.objects.filter(user__username=request.user).count()
        context['Error'] = 'Playlist is not created'
        return HttpResponseRedirect('/profile/', context)

def removeSong(request, song_id):
    context = {}
    song = Song.objects.get(pk=song_id)
    # user = User.objects.get(username=request.user)
    playlist = Playlist.objects.get_or_create(playlist_user=request.user)[0]
    playlist.playlist_songs.remove(song)
    context['playlist'] = Playlist.objects.get(playlist_user=request.user).playlist_songs.all()
    return render(request, 'Playlist.html', context)

def addSong(request, song_id):
    context = {}

    song = Song.objects.get(pk=song_id)
    user = User.objects.get(username=request.user)
    playlist = Playlist.objects.get_or_create(playlist_user=user)[0]
    playlist.playlist_songs.add(song)

    context['songs'] = homepage_parameters(request)
    return render(request, 'index.html', context)

def likeSong(request, song_id):
    context = {}
    song = Song.objects.get(pk=song_id)
    user = User.objects.get(username=request.user)
    song.likes.add(user)
    song.num_likes += 1
    song.save()
    context['songs'] = homepage_parameters(request)
    return render(request, 'index.html', context)

def dislikeSong(request, song_id):
    context = {}
    song = Song.objects.get(pk=song_id)
    user = User.objects.get(username=request.user)
    song.likes.remove(user)
    song.num_likes -= 1
    song.save()
    context['songs'] = homepage_parameters(request)
    return render(request, 'index.html', context)

def topSongs(request):
    context = {}
    context['songs'] = Song.objects.order_by('-num_likes')[:5]
    return render(request, 'topSongs.html', context)

def genrePage(request):
    context = {}
    genres_list = ['Pop', 'Rock', 'House', 'Hip-Hop', 'Techno', 'Unknown']
    genres = []
    genres_count = []
    for genre in genres_list:
        genres.append(genre)
        # results.append(genre + ' ('+ str(Song.objects.filter(song_genre=genre).count())+')')
        genres_count.append(Song.objects.filter(song_genre=genre).count())
    genres_info = zip(genres, genres_count)
    context['genre_list'] = genres_info
    context['genres_count'] = genres_count
    return render(request, 'genrePage.html', context)

def displayGenre(request, genre):
    context = {}
    print(genre)
    user_likes = []
    flag = False
    for song in Song.objects.filter(song_genre=genre):
        for user in song.likes.all():
            if user == request.user:
                flag = True
                break
        if flag:
            user_likes += ['True']
        else:
            user_likes += ['False']
        flag = False
    context['songs'] = zip(Song.objects.filter(song_genre=genre), user_likes)
    context['comments'] = Comment.objects.filter(song__song_genre=genre)
    return render(request, 'displayGenre.html', context)

def comments(request, song_id):
    context = {}
    form = CommentForm(request.POST or None)
    if form.is_valid():
        print('p')
        if len(str(form.cleaned_data['comment'])) > 120:
            context = {
                        'form': form,
                        'error_message': 'Comment can not exceed 120 characters limit',
                        'comments': Comment.objects.filter(song__pk=song_id)
                    }
            return render(request, 'comments.html', context)

        comment = form.save(commit=False)
        comment.song = Song.objects.get(pk=song_id)
        comment.user = request.user
        comment.comment = form.cleaned_data['comment']
        comment.save()
        context['comments'] = Comment.objects.filter(song__pk=song_id)
        context['form'] = form
        return redirect('/comments/'+str(song_id), context)

    context['comments'] = Comment.objects.filter(song__pk=song_id)
    context['form'] = form
    return render(request, 'comments.html', context)

def editProfile(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/')
    user = User.objects.get(username=request.user)
    form = EditProfile(request.POST or None, initial={'first_name': user.first_name, 'last_name': user.last_name, 'email': user.email})
    if form.is_valid():
        user.first_name = form.cleaned_data['first_name']
        user.last_name = form.cleaned_data['last_name']
        user.email = form.cleaned_data['email']
        user.save()

        context = {}
        context['user_info'] = User.objects.get(username__exact=request.user)
        context['songs'] = Song.objects.filter(user__username=request.user)
        context['uploaded_songs'] = Song.objects.filter(user__username=request.user).count()
        try:
            context['playlist'] = Playlist.objects.get(playlist_user=request.user).playlist_songs.all()
        except:
            pass
        return HttpResponseRedirect('/profile/', context)

    context = {
        'form': form,
    }
    return render(request, 'EditProfileInfo.html', context)

