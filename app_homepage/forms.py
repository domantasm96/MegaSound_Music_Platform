from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from app_homepage.models import *

class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)


    class Meta:
        model = User
        fields = ['username', 'password', 'first_name', 'last_name', 'email']

class SongForm(forms.ModelForm):

    class Meta:
        model = Song
        fields = ['song_author', 'song_title', 'song_genre', 'song_file']

class SongEditForm(forms.ModelForm):

    class Meta:
        model = Song
        fields = ['song_author', 'song_title', 'song_genre']

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['comment']

class EditProfile(forms.ModelForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']