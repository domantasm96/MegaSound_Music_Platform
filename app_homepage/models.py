# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import User

from django.utils import timezone

GENRES_NAMES = (
    ('Pop', 'Pop'),
    ('Rock', 'Rock'),
    ('House', 'House'),
    ('Hip-Hop', 'Hip-Hop'),
    ('Techno', 'Techno'),
    ('Unknown', 'Unknown'),
)

def file_size(value):  # add this to some file where you can import it from
    limit = 10 * 1024 * 1024
    if value.size > limit:
        raise ValidationError('File too large. Size should not exceed {} MiB.'.format(limit / 1024 / 1024))


class Song(models.Model):
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    song_author = models.CharField(max_length=200, default='')
    song_title = models.CharField(max_length=200)
    song_genre = models.CharField(max_length=200, default='Unknown', choices=GENRES_NAMES)
    pub_date = models.DateTimeField('date published', default=timezone.now)
    likes = models.ManyToManyField(User, related_name='song_likes', blank=True, null=True)
    song_file = models.FileField(default='', validators=[file_size])
    num_likes = models.IntegerField(default=0)

    def __str__(self):
        return "{}: {} - {} | {}".format(self.user, self.song_author, self.song_title, self.pub_date)


class Playlist(models.Model):
    playlist_user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
    playlist_songs = models.ManyToManyField(Song)
    creation_date = models.DateTimeField('Playlist created', auto_now_add=True)

    def __str__(self):
        return self.playlist_user.username

class Comment(models.Model):
    song = models.ForeignKey(Song, blank=True, null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.CASCADE)
    comment = models.TextField(max_length=120)
    creation_date = models.DateTimeField('Comment date', default=timezone.now)

    def __str__(self):
        return "{}: {}| {}".format(self.user, self.song, self.creation_date)

