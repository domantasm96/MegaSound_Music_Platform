from django.conf.urls import url

from . import views

app_name = 'app_homepage'

urlpatterns = [
    url(r'^$', views.homepage, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.login_user, name='login'),
    url(r'^logout/$', views.logout_user, name='logout'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^profile/(?P<profile_name>[\w\-]+)/$', views.otherProfile, name='otherProfile'),
    url(r'^upload/$', views.upload_song, name='upload_song'),
    url(r'^delete/(?P<song_id>[\w\-]+)/$', views.delete_song, name='delete_song'),
    url(r'^playlist/$', views.playlist, name='playlist'),
    url(r'^addSong/(?P<song_id>[\w\-]+)/$', views.addSong, name='addSong'),
    url(r'^removeSong/(?P<song_id>[\w\-]+)/$', views.removeSong, name='removeSong'),
    url(r'^likeSong/(?P<song_id>[\w\-]+)/$', views.likeSong, name='likeSong'),
    url(r'^dislikeSong/(?P<song_id>[\w\-]+)/$', views.dislikeSong, name='dislikeSong'),
    url(r'^editSong/(?P<song_id>[\w\-]+)/$', views.edit_song, name='editSong'),
    url(r'^top5/$', views.topSongs, name='topSongs'),
    url(r'^comments/(?P<song_id>[\w\-]+)/$', views.comments, name='comments'),
    url(r'^genrePage/$', views.genrePage, name='genrePage'),
    url(r'^genrePage/(?P<genre>[\w\-]+)/$', views.displayGenre, name='displayGenre'),
    url(r'^editProfile/$', views.editProfile, name='editProfile'),
]

