# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *
admin.site.register(Song)
admin.site.register(Playlist)
admin.site.register(Comment)