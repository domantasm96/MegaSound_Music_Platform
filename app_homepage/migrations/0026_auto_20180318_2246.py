# Generated by Django 2.0.2 on 2018-03-18 20:46

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app_homepage', '0025_auto_20180318_2034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='song',
            name='pub_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='date published'),
        ),
    ]
