# Generated by Django 2.0.2 on 2018-02-17 22:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('app_homepage', '0002_auto_20180217_2201'),
    ]

    operations = [
        migrations.AddField(
            model_name='playlist',
            name='playlist_user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='playlist_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
